# Convolution Neural Network (CNN / ConvNet)

![CNN Structure](../CNN_Images/cnn.jpeg)

CNN are made up of neurons that have learnable weights and biases. Each neurons receives some inputs, performs a dot product and optionally follows it with a non-linearity. 
The whole network still expresses a single differentiable score function: from the raw image pixels on one end to class scores at the other.

## Architecture Overview

Neural Networks receive an input (a single vector), and transform it through a series of hidden layers. Each hidden layer is made up of a set of neurons, where each neuron is fully connected to all neurons in the previous layer, and where neurons in a single layer function completely independently and do not share any connections. 
The last fully-connected layer is called the “output layer” and in classification settings it represents the class scores.

<div align="center">
<img src='../CNN_Images/cnn_sequence.jpeg' >
<p>A CNN sequence to classify handwritten digits</p>
</div>

> A ConvNet is made up of Layers. Every Layer has a simple API: It transforms an input 3D volume to an output 3D volume with some differentiable function that may or may not have parameters.

## Why ConvNets over Feed-Forward Neural Nets?

<div align="center">
<img src='../CNN_Images/matrix.png' >
<p>Flattening of a 3x3 image matrix into a 9x1 vector</p>
</div>


An image is nothing but a matrix of pixel values, right? So why not just flatten the image (e.g. 3x3 image matrix into a 9x1 vector) and feed it to a Multi-Level Perceptron for classification purposes?

In cases of extremely basic binary images, the method might show an average precision score while performing prediction of classes but would have little to no accuracy when it comes to complex images having pixel dependencies throughout.

## Input Image 

<div align="center">
<img src='../CNN_Images/rgb.png' >
<p>4x4x3 RGB Image</p>
</div>

In the figure, we have an RGB image which has been separated by its three color planes- Red, Green, and Blue. There are a number of such color spaces in which images exist- Grayscale, RGB, HSV, CMYK, etc.

>  The role of the ConvNet is to reduce the images into a form which is easier to process, without losing features which are critical for getting a good prediction.

## Convolution Layer - The Kernel

<div align="center">
<img src='../CNN_Images/convolution layer.gif' >
<p>Convoluting a 5x5x1 image with a 3x3x1 kernel to get a 3x3x1 convolved feature</p>
</div>


Image Dimensions = 5 (Height) x 5 (Breadth) x 1 (Number of channels, eg. RGB)


In the above demonstration, the green section resembles our 5x5x1 input image, I. The element involved in carrying out the convolution operation in the first part of a Convolutional Layer is called the Kernel/Filter, K, represented in the color yellow. We have selected K as a 3x3x1 matrix.

```
Kernel/Filter, K = 

1  0  1
0  1  0
1  0  1
```


The Kernel shifts 9 times because of Stride Length = 1 (Non-Strided), every time performing a matrix multiplication operation between K and the portion P of the image over which the kernel is hovering.

<div align="center">
<img src='../CNN_Images/movement of kernel.png'" >
<p>Movement of the Kernel</p>
</div>

The filter moves to the right with a certain Stride Value till it parses the complete width. Moving on, it hops down to the beginning (left) of the image with the same Stride Value and repeats the process until the entire image is traversed.

<div align="center">
<img src='../CNN_Images/cnn_matrix.gif'" >
<p>Convolution operation on a MxNx3 image matrix with a 3x3x3 Kernel</p>
</div>

In the case of images with multiple channels (e.g. RGB), the Kernel has the same depth as that of the input image. 

Matrix Multiplication is performed between **Kn** and **In** stack *([K1, I1]; [K2, I2]; [K3, I3])* and all the results are summed with the bias to give us a squashed one-depth channel Convoluted Feature Output.

<div align="center">
<img src='../CNN_Images/cnn_operation.gif'" >
<p>Convolution Operation with Stride Length = 2</p>
</div>

> The objective of the Convolution Operation is to extract the high-level features such as edges, from the input image. 

There are two types of results to the operation - one in which the convolved feature is reduced in dimensionality as compared to the input, and the other in which the dimensionality is either increased or remains the same. 
This is done by applying Valid Padding in case of the former, or Same Padding in the case of the latter.

<div align="center">
<img src='../CNN_Images/padding.gif'" >
<p>SAME padding: 5x5x1 image is padded with 0s to create a 6x6x1 image</p>
</div>

1. When we augment the 5x5x1 image into a 6x6x1 image and then apply the 3x3x1 kernel over it, we find that the convolved matrix turns out to be of dimensions 5x5x1. Hence the name- **Same Padding**.

2. On the other hand, if we perform the same operation without padding, we are presented with a matrix which has dimensions of the Kernel (3x3x1) itself- **Valid Padding**.

## Pooling Layer

<div align="center">
<img src='../CNN_Images/pooling.gif'" >
<p>3x3 pooling over 5x5 convolved feature</p>
</div>

Similar to the Convolutional Layer, the Pooling layer is responsible for reducing the spatial size of the Convolved Feature. This is to decrease the computational power required to process the data through dimensionality reduction. Furthermore, it is useful for extracting dominant features which are rotational and positional invariant, thus maintaining the process of effectively training of the model.

There are two types of Pooling:

**1. Max Pooling**- Max Pooling returns the maximum value from the portion of the image covered by the Kernel. It also performs as a Noise Suppressant and discards the noisy activations altogether and also performs de-noising along with dimensionality reduction.

**2. Average Pooling**- Average Pooling returns the average of all the values from the portion of the image covered by the Kernel. It simply performs dimensionality reduction as a noise suppressing mechanism.


<div align="center">
<img src='../CNN_Images/types of pooling.png'" >
<p>Types of Pooling</p>
</div>


> Hence, we can say that Max Pooling performs a lot better than Average Pooling.

## Classification — Fully Connected Layer (FC Layer)

<div align="center">
<img src='../CNN_Images/fully connected layer.jpeg'>
</div>

Adding a Fully-Connected layer is a (usually) cheap way of learning non-linear combinations of the high-level features as represented by the output of the convolutional layer. The Fully-Connected layer is learning a possibly non-linear function in that space.
Over a series of epochs, the model is able to distinguish between dominating and certain low-level features in images and classify them using the Softmax Classification technique.

There are various architectures of CNNs available which have been key in building algorithms which power and shall power AI as a whole in the foreseeable future. Some of them have been listed below:
1. LeNet
2. AlexNet
3. VGGNet
4. GoogLeNet
5. ResNet
6. ZFNet


### References- 
1. [Towards Data Science](https://towardsdatascience.com/)
2. [Medium](https://medium.com/)
            
















