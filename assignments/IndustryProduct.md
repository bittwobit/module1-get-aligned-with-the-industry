# Industry Product Study

## Product-1

### Product Name

AgroScout

### Product Link

[AgroScout](https://agro-scout.com/platforms/)

### Product Short Description

AgroScout is a startup in the agricultural technology (agritech) sector dedicated to the early detection of pests and disease in field crops. And, it’s a prime example of a cutting-edge company that’s using Oracle Cloud Native Services to migrate their application to Kubernetes and deliver an automated deployment pipeline.

<div align="center">
<img src='../Industry_Product_Images/AgroScout/workflow.png' >
<p>Workflow in AgroScout</p>
</div>

Field scouts are now using the AgroScout AI drone platform to find bugs and disease faster

1. GPS tagged image report
2. Scout 50 acres in 20 min
3. Picks up late and early blight etc,
4. Better detection means less spraying

**Demo Video-**   [Link](https://www.youtube.com/watch?v=seS3H6LRlSs)

### Product is combination of features

AgroScout surveys fields by using autopiloted drones with cameras. It then processes, detects, and classifies any issues with the crops before recommending treatment. The company relies on Graphical Processing Unit (GPU) based Machine Learning and a set of microservices backed by a SQL database.

Platforms used by them are-

1. Scouting App

<div align="center">
<img src='../Industry_Product_Images/AgroScout/scouting_app.png' >
</div>

2. Web-Based AI Reporting & Analysis


<div align="center">
<img src='../Industry_Product_Images/AgroScout/platforms_web_based.png' >
</div>

3. GPS Tagged Findings

<div align="center">
<img src='../Industry_Product_Images/AgroScout/gps_tagged.png' >
</div>

### Product is provided by which company?

AgroScout

## Product-2

### Product Name
Locale.ai

### Product Link

[Locale.ai](https://www.locale.ai/product)

### Product Short Description

Real-time operational analytics platform using location data.
Analyze where you can-

1. reduce cost per delivery

<div align="center">
<img src='../Industry_Product_Images/localeai/logistics_supply_chain.png' >
<p>Supply Chain</p>
</div>

2. increase user conversion

<div align="center">
<img src='../Industry_Product_Images/localeai/mobility.png' >
<p>Mobility</p>
</div>

3. increase asset utilization

<div align="center">
<img src='../Industry_Product_Images/localeai/on_demand.png' >
<p>On Demand Delivery</p>
</div>

4. improve unit economics

<div align="center">
<img src='../Industry_Product_Images/localeai/workforce.png' >
<p>Workforce</p>
</div>

This product helps to improve your most critical metrices using ground truth like-

1. Real-time integration across databases
2. All your location data & metrics in one place
3. Granular Insights at Terabyte Scale
4. Workflows & Actions to Close the Loop

### Product is combination of features

1. Cohorts- Analyzing the behavior of a group
2. LookUps- Profile a single user or a vehicle
3. Clustering- Segregating areas that are similar
4. Anomalies- Detecting abnormal patterns & outliers

### Product is provided by which company?

Locale.ai