# Deep Neural Network (DNN)

<div align="center">
<img src='../DNN_Images/LeNet.png' >
<p>Neural Network</p>
</div>

Deep learning models, in simple words, are large and deep artificial neural nets. A neural network (“NN”) can be well presented in a directed acyclic graph: the input layer takes in signal vectors; one or multiple hidden layers process the outputs of the previous layer.

<div align="center">
<img src='../DNN_Images/ANN.png' >
<p>A three-layer artificial neural network</p>
</div>

A large and deep neural network has many more layers + many more nodes in each layer, which results in exponentially many more parameters to tune. Without enough data, we cannot learn parameters efficiently. Without powerful computers, learning would be too slow and insufficient.

Here is an interesting plot presenting the relationship between the data scale and the model performance, proposed by Andrew Ng in his talk named **Nuts and Bolts of Applying Deep Learning**.

<div align="center">
<img src='../DNN_Images/data_size_vs_model_performance.png' >
<p>The data scale versus the model performance</p>
</div>

## Neural Network Elements

A node combines input from the data with a set of coefficients, or weights, that either amplify or dampen that input, thereby assigning significance to inputs with regard to the task the algorithm is trying to learn. These input-weight products are summed and then the sum is passed through a node’s so-called activation function, to determine whether and to what extent that signal should progress further through the network to affect the ultimate outcome, say, an act of classification. If the signals passes through, the neuron has been “activated.”

<div align="center">
<img src='../DNN_Images/perceptron_node.png' >
<p>Perception Node</p>
</div>

A node layer is a row of those neuron-like switches that turn on or off as the input is fed through the net. Each layer’s output is simultaneously the subsequent layer’s input, starting from an initial input layer receiving your data.

<div align="center">
<img src='../DNN_Images/mlp.png' >
<p>Types of layers</p>
</div>

Pairing the model’s adjustable weights with input features is how we assign significance to those features with regard to how the neural network classifies and clusters input.

## Key Concepts of Deep Neural Networks

1. Deep Learning networks are distinguished from the more commonplace single-hidden-layer neural networks by their depth; that is, the number of node layers through which data must pass in a multistep process of pattern recognition.

2. In deep learning networks, each layer of nodes trains on a distinct set of features based on the previous layer’s output. The further you advance into the neural net, the more complex the features your nodes can recognize, since they aggregate and recombine features from the previous layer.

<div align="center">
<img src='../DNN_Images/feature_hierarchy.png' >
<p>Feature Hierarchy</p>
</div>

>This is known as feature hierarchy, and it is a hierarchy of increasing complexity and abstraction. It makes deep-learning networks capable of handling very large, high-dimensional data sets with billions of parameters that pass through nonlinear functions.

3. Deep Learning networks perform automatic feature extraction without human intervention, unlike most traditional machine-learning algorithms. Given that feature extraction is a task that can take teams of data scientists years to accomplish, deep learning is a way to circumvent the chokepoint of limited experts. It augments the powers of small data science teams, which by their nature do not scale.

4. Deep Learning networks end in an output layer: a logistic, or softmax, classifier that assigns a likelihood to a particular outcome or label. We call that predictive, but it is predictive in a broad sense. Given raw data in the form of an image, a deep-learning network may decide, for example, that the input data is 90 percent likely to represent a person.

## Gradient Descent

Gradient is another word for slope, and slope, in its typical form on an x-y graph, represents how two variables relate to each other: *rise over run*, the *change in money over the change in time*, etc. In this particular case, the slope we care about describes the relationship between the network’s error and a single weight; i.e. that is, how does the error vary as the weight is adjusted.

Each weight is just one factor in a deep network that involves many transforms; the signal of the weight passes through activations and sums over several layers, so we use the chain rule of calculus to march back through the networks activations and outputs and finally arrive at the weight in question, and its relationship to overall error.

The chain rule in calculus states that

<div align="center">
<img src='../DNN_Images/chain_rule.png' >
</div>

In a feedforward network, the relationship between the net’s error and a single weight will look something like this:

<div align="center">
<img src='../DNN_Images/backprop_chain_rule.png' >
</div>

That is, given two variables, Error and weight, that are mediated by a third variable, activation, through which the weight is passed, you can calculate how a change in weight affects a change in Error by first calculating how a change in activation affects a change in Error, and how a change in weight affects a change in activation.


<div align="center">
<img src='../DNN_Images/gradient_descent.png' >
</div>




## Visual Cortex System and Logistic Regression

CNN is a type of feed-forward artificial neural networks, in which the connectivity pattern between its neurons is inspired by the organization of the visual cortex system. The primary visual cortex (V1) does edge detection out of the raw visual input from the retina. The secondary visual cortex (V2), also called prestriate cortex, receives the edge features from V1 and extracts simple visual properties such as orientation, spatial frequency, and color. 

<div align="center">
<img src='../DNN_Images/visual_cortex_system.png' >
<p>Illustration of the human visual cortex system</p>
</div>

The visual area V4 handles more complicated object attributes. All the processed visual features flow into the final logic unit, inferior temporal gyrus (IT),for object recognition. The shortcut between V1 and V4 inspires a special type of CNN with connections between non-adjacent layers: Residual Net  containing “Residual Block” which supports some input of one layer to be passed to the component two layers later.

On a DNN of many layers, the final layer has a particular role. When dealing with labeled input, the output layer classifies each example, applying the most likely label. Each node on the output layer represents one label, and that node turns on or off according to the strength of the signal it receives from the previous layer’s input and parameters.

The mechanism we use to convert continuous signals into binary output is called logistic regression. The name is unfortunate, since logistic regression is used for classification rather than regression in the linear sense that most people are familiar with. It calculates the probability that a set of inputs match the label.

<div align="center">
<img src='../DNN_Images/logistic_regression.png' >
</div>

## Back Propagation

In this case, we used only one layer inside the neural network between the inputs and the outputs. We can always create one complicated function that represent the composition over the whole layers of the network. For instance, if layer 1 is doing: 3.x to generate a hidden output z, and layer 2 is doing: z² to generate the final output, the composed network will be doing (3.x)² = 9.x².

<div align="center">
<img src='../DNN_Images/paths.png' >
</div>

In order to solve the problem, luckily for us, derivative is decomposable, thus can be back-propagated.
We have the starting point of errors, which is the loss function, and we know how to calculate its derivative, and if we know how to calculate the derivative of each function from the composition, we can propagate back the error from the end to the start.
Let’s consider the simple linear example: where we multiply the input 3 times to get a hidden layer, then we multiply the hidden (middle layer) 2 times to get the output.

<div align="center">
<img src='../DNN_Images/forward_and_backward_path.png' >
<p>Diagram of Forward and Backward Paths </p>
</div>

This figure shows the process of back-propagating errors following this schemas:
Input -> Forward calls -> Loss function -> derivative -> back-propagation of errors. At each stage we get the deltas on the weights of this stage.

<div align="center">
<img src='../DNN_Images/backpropagation.gif' >
<p>Animation for the Forward and Backward Paths </p>
</div>

### Example

The AlphaGo system starts with a supervised learning process to train a fast rollout policy and a policy network, relying on the manually curated training dataset of professional players’ games. It learns what is the best strategy given the current position on the game board. Then it applies reinforcement learning by setting up self-play games. The RL policy network gets improved when it wins more and more games against previous versions of the policy network. In the self-play stage, AlphaGo becomes stronger and stronger by playing against itself without requiring additional external training data.

<div align="center">
<img src='../DNN_Images/alphago_model.png' >
<p>AlphaGo Neural Network Training Pipeline and Architecture</p>
</div>


### References-
1. [PathMind](https://pathmind.com/wiki/neural-network)
2. [Lil'Log](https://lilianweng.github.io/lil-log/2017/06/21/an-overview-of-deep-learning.html)
3. [Medium](https://medium.com/datathings/neural-networks-and-backpropagation-explained-in-a-simple-way-f540a3611f5e)


















